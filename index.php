<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Case Clicker hacks</title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

    <script>
        var userID = "A88LB5PBAMC8YD50754";
        var tradeID;

        $(document).ready(function () {
            $('#indexItemName').on("keydown",function(e){
                if (e.which == 13) {
                    index($('#indexItemID').val(), $('#indexItemName').val());
                }
            });
        })



        function setUid(uid) {
            userID = uid;
            $('#userID').text("UserID: " + userID);
        }

        function addRafflePoints(points) {
            $.get("hackBackend.php", {
                uid: userID,
                points: points
            }, function (data, status) {
                if(status === "success"){
                    $('#pointCount').text("Current points: " + data);
                }else{
                    alert("Could not add raffle points!")
                }
            });
        }

        function createTrade(trader) {
            $.get("hackBackend.php",{
                uid: userID,
                trader: trader
            }, function (data, status) {
               if(status === "success"){
                   tradeID = data;
                    $('#tradeID').text("TradeID: " + data);
               }else{
                   $('#tradeID').text("Could not create trade!");
               }
            });
        }

        function addItem(id, type, rarity, stat, quality, amount) {
            var arg = id + ";" + type + ";" + rarity + ";" + stat + ";" + quality;

            if(!amount) amount = 1;

            $.get("hackBackend.php",{
                uid: userID,
                addItem: arg,
                tradeID: tradeID,
                amount: amount
            }, function (data, status) {
                if(status === "success"){
                    var json = JSON.parse(data);
                    $('#itemCount').text("Items in trade: " + json.ownItems.length);
                }else{
                    alert("Could not add Item!");
                }
            });


        }

        function autoAddItems(startID) {
            $.get("autoAdd.php", {
                auto: true,
                startID: startID,
                tradeID: tradeID,
                uid: userID
            }, function (data, status) {
                if(status === "success"){
                    var json = JSON.parse(data);
                    $('#itemCount').text("Items in trade: " + json.ownItems.length);
                }else{
                    alert("Could not add Item!");
                }
            })
        }

        function index(id, name) {
            $.get("itemIndex.php", {
                id: id,
                name: name
            }, function (data, status) {
                if(status === "success"){
                    $('#indexItemID').val(function (i, oldval) {
                        return ++oldval;
                    })

                }else{
                    alert("Could not add Item!");
                }
            })
        }

    </script>

    <style>

        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }

        th, td {
            padding: 15px;
        }

        th {
            text-align: left;
        }
    </style>

</head>
<body>

<h3>Set UserID:</h3>
<input type="text" id="uid" value="A88LB5PBAMC8YD50754">
<input type="button" value="set" onclick="setUid($('#uid').val())"><br>
<a id="userID"></a>
<h3>Add raffle points:</h3>
    <input type="number" id="points" placeholder="points to add">
    <input type="button" value="add" onclick="addRafflePoints($('#points').val())">
    <br>
    <a id="pointCount"></a>

<div>
    <h3>Create fake trade:</h3>
    <input type="text" placeholder="Sauerbier" id="trader">
    <input type="button" value="create" onclick="createTrade($('#trader').val())">
    <br>
    <a id="tradeID"></a>
</div>

<h3>Add items to fake trade:</h3>
<label for="id">ItemID</label>
<input id="id" type="number" value="199"><br>
<label for="type">ItemType</label>
<input id="type" type="number" value="3" min="1" max="7"><br>
<label for="rarity">Rarity</label>
<input id="rarity" type="number" value="4" min="-1" max="5"><br>
<label for="stat">Stattrack?</label>
<input id="stat" type="number" value="1" min="0" max="1"><br>
<label for="quality">Quality</label>
<input id="quality" type="number" value="1" min="1" max="6"><br>
<input type="button" value="add" onclick="addItem($('#id').val(),$('#type').val(),$('#rarity').val(),$('#stat').val(),$('#quality').val(),$('#itemMulti').val())">
<input type="number" id="itemMulti" placeholder="add x times" max="20" min="1"><br>

<input type="button" value="autoAdd" onclick="autoAddItems($('#autoAdd').val())">
<input type="number" id="autoAdd" placeholder="startID"><br>
<a id="itemCount"></a><br><br>
<input type="number" id="indexItemID">
<input type="text" id="indexItemName">
<input type="button" value="index" onclick="index($('#indexItemID').val(), $('#indexItemName').val())">
<br><br><br><br><br><br><br>
<table>
    <tr>
        <th>id</th>
        <th>type</th>
    </tr>
    <tr>
        <td>7</td>
        <td>Sticker</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Weapons/Knifes</td>
    </tr>
</table>
<br><br>

<table style="width:100%" class="items">
    <?php
    include_once 'MySQL.php';


    $stmt = $mysqli->prepare("select * from items ORDER by id ASC ");
    if($stmt){
        $stmt->execute();
        $stmt->store_result();

        for ($i = 0; $i < $stmt->num_rows; $i++){
            $stmt->bind_result ( $id, $name );
            $stmt->fetch ();

            echo ("<tr>
                    <td>$id</td>
                    <td>$name</td>
                   </tr>");
        }
    }
    ?>
</table>
</body>
</html>