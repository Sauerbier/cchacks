<?php
/**
 * Created by PhpStorm.
 * User: jan
 * Date: 26.02.17
 * Time: 18:21
 * @param $method
 * @param $url
 * @param bool $data
 * @return mixed
 */
include_once 'MySQL.php';

function callAPI($method, $url, $data = false)
{
    $curl = curl_init();

    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);

            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }


    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    $result = curl_exec($curl);

    curl_close($curl);

    return $result;
}

function addTradeItem($itemID, $type, $rarity, $stat, $quality, $tradeID, $amount,$uid ){
    if($amount > 1){
        $json = "[";
        for($i = 0; $i < $amount; $i++){
            $json .= "{\"type\":".$type.",\"rarity\":".$rarity.",\"id\":".$itemID.",\"stattrak\":".$stat.",\"equipment_id\":56,\"quality\":".$quality."},";
        }
        $json = substr($json,0,strlen($json) -1);
        $json .= "]";
    }else{
        $json = "[{\"type\":".$type.",\"rarity\":".$rarity.",\"id\":".$itemID.",\"stattrak\":".$stat.",\"equipment_id\":56,\"quality\":".$quality."}]";
    }

   return callAPI("POST", "http://caseclicker.eu/trades/additem/".$uid."/" . $tradeID, array("items" => $json));
}

function addJsonItem($old, $itemID, $type, $rarity, $stat, $quality, $amount){
    if($amount > 1){
        $json = empty($old) ? "[" : $old;
        for($i = 0; $i < $amount; $i++){
            $json .= "{\"type\":".$type.",\"rarity\":".$rarity.",\"id\":".$itemID.",\"stattrak\":".$stat.",\"equipment_id\":56,\"quality\":".$quality."},";
        }
        $json = substr($json,0,strlen($json) -1);
        $json .= "]";
    }else{
        $json = empty($old) ? "[" : $old;
        $json .= "{\"type\":".$type.",\"rarity\":".$rarity.",\"id\":".$itemID.",\"stattrak\":".$stat.",\"equipment_id\":56,\"quality\":".$quality."}";
        if(empty($old)) $json .= "]";
    }

    return $json;
}



if(isset($_GET['trader']) && isset($_GET['uid'])){
    $req = callAPI("get", "http://caseclicker.eu/trades/create/".$_GET['uid']."/" . $_GET['trader']);
    $json = json_decode($req);
    $trade = $json->trade->id;
    $_SESSION['tradeID'] = $trade;
    echo $trade;
    exit;
}


if(isset($_GET['addItem']) && isset($_GET['tradeID']) && isset($_GET['amount']) && isset($_GET['uid'])){
    $itemID = explode(';',$_GET['addItem'])[0];
    $type = explode(';',$_GET['addItem'])[1];
    $rarity = explode(';',$_GET['addItem'])[2];
    $stat = explode(';',$_GET['addItem'])[3];
    $quality = explode(';',$_GET['addItem'])[4];

    $json = addTradeItem($itemID,$type,$rarity,$stat,$quality, $_GET['tradeID'], $_GET['amount'], $_GET['uid']);
    echo $json;
    exit;
}


if(isset($_GET['points']) && isset($_GET['uid'])){
    callAPI("POST", "http://178.216.200.219/raffles/addpoints/".$_GET['uid'],"points=".$_GET['points']);
    $json = json_decode(callAPI("GET", "http://178.216.200.219/raffles/login/".$_GET['uid']));
    echo $json->points;
    exit;
}




